/* config.h.  Generated by hand for Win32 using Visual C++ 6.0. */

/* Define if you have the ANSI C header files.  */
#define STDC_HEADERS 1
#undef PACKAGE
#undef VERSION

/* Define if you have the erf function.  */
#undef HAVE_ERF

/* Define if you have the snprintf function.  */
#undef HAVE_SNPRINTF

/* Work around for broken for-scoping with Visual C++ 6.0 */
#pragma warning(disable:4127)
#define for if(0);else for

/* disable some stupid warnings */
#pragma warning(disable:4786 4800)

/* enable a bunch of useful warnings */
#pragma warning(3:4019 4032 4057 4061 4125 4130 4152 4189 4201 4706)

#define GOOSE_MAJOR_VERSION 0
#define GOOSE_MINOR_VERSION 0
#define GOOSE_MICRO_VERSION 5


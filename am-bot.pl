#!/usr/bin/perl -w
# $Id$

print STDERR "The script \"am-bot.pl\" is no longer used.\n";
exit;

### This script is deep in copy-and-modify hell


### Generate Makefile.am for specfns dir

print STDERR "Building specfns/Makefile.am... ";

my @specfns_src = map { /\/([^\/]+)\.cpp$/ } <specfns/*.cpp>;
my @specfns_hdr = map { /\/([^\/]+)\.h$/ } <specfns/*.h>;

open(OUT, ">specfns/Makefile.am") || die;

print OUT <<'EOF';
### This is an automatically generated Makefile.am file ###

INCLUDES = -I$(includedir)

lib_LTLIBRARIES = libspecfns.la

libspecfns_la_SOURCES = \
EOF

print OUT join(" \\\n", map { "\t$_.cpp" } @specfns_src), "\n\n";

print OUT <<'EOF';
libspecfnsincludedir = $(includedir)/goose

libspecfnsinclude_HEADERS = \
EOF

print OUT join(" \\\n", map { "\t$_.h" } @specfns_hdr), "\n\n";

print OUT <<EOF;
libspecfns_la_LDFLAGS = -version-info \$(GOOSE_CURRENT):\$(GOOSE_REVISION):\$(GOOSE_AGE)

EOF

close(OUT);
print STDERR "Done\n";




### Generate Makefile.am for src dir

print STDERR "Building src/Makefile.am... ";

my @lib_src = map { /\/([^\/]+)\.cpp$/ } <src/*.cpp>;
my @lib_hdr = map { /\/([^\/]+)\.h$/ } <src/*.h>;

open(OUT, ">src/Makefile.am") || die;

print OUT <<'EOF';
### This is an automatically generated Makefile.am file ###

INCLUDES = -I$(top_srcdir)/specfns -I$(includedir)

lib_LTLIBRARIES = libgoose.la

libgoose_la_SOURCES = \
EOF

print OUT join(" \\\n", map { "\t$_.cpp" } @lib_src), "\n\n";

print OUT <<'EOF';
libgooseincludedir = $(includedir)/goose

libgooseinclude_HEADERS = \
EOF

print OUT join(" \\\n", map { "\t$_.h" } @lib_hdr), "\n\n";

print OUT <<EOF;
libgoose_la_LDFLAGS = -version-info \$(GOOSE_CURRENT):\$(GOOSE_REVISION):\$(GOOSE_AGE)

EOF

close(OUT);
print STDERR "Done\n";


### Generate Makefile.am for examples dir

print STDERR "Building examples/Makefile.am... ";

my @example_src = map { /\/([^\/]+)\.cpp$/ } <examples/*.cpp>;

open(OUT, ">examples/Makefile.am") || die;

print OUT <<'EOF';
### This is an automatically generated Makefile.am file ###

## Include from the src directory
INCLUDES = -I$(top_srcdir)/specfns -I$(top_srcdir)/src -I$(includedir)
LDADD = $(top_srcdir)/src/libgoose.la $(top_builddir)/specfns/libspecfns.la

## By doing this, we trick libtool into always using the most recently
## compiler version of our shared lib, the one in ../src/.libs, as the
## first hard-wired path in the generated executable.
LDFLAGS = -Wl,-rpath -Wl,../src/.libs -Wl,-rpath -Wl,../specfns/.libs 

noinst_PROGRAMS =  \
EOF

print OUT join(" \\\n", map { "\t$_" } @example_src), "\n\n";

foreach (@example_src) {
    my $canonize = $_;
    $canonize =~ tr /\-/_/;
    print OUT $canonize, "_SOURCES = $_.cpp\n";
}

print OUT "\n";

#foreach (@example_src) {
#    my $canonize = $_;
#    $canonize =~ tr /\-/_/;
#    print OUT $canonize,
#    '_LDADD = $(top_builddir)/src/libgoose.la', "\n";
#}

close(OUT);
print STDERR "Done\n";

### Generate Makefile.am for test dir

print STDERR "Building test/Makefile.am... ";

my @test_src = map { /\/([^\/]+)\.cpp$/ } <test/*.cpp>;

open(OUT, ">test/Makefile.am") || die;

print OUT <<'EOF';
### This is an automatically generated Makefile.am file ###

## Include from the src directory
INCLUDES = -I$(top_builddir)/specfns -I$(top_builddir)/src -I$(includedir)
LDADD = $(top_builddir)/src/libgoose.la $(top_builddir)/specfns/libspecfns.la

EXTRA_DIST = runtests.pl README

## By doing this, we trick libtool into always using the most recently
## compiler version of our shared lib, the one in ../src/.libs, as the
## first hard-wired path in the generated executable.
LDFLAGS = -Wl,-rpath -Wl,../src/.libs -Wl,-rpath -Wl,../specfns/.libs 

noinst_PROGRAMS =  \
EOF

print OUT join(" \\\n", map { "\t$_" } @test_src), "\n\n";

foreach (@test_src) {
    my $canonize = $_;
    $canonize =~ tr /\-/_/;
    print OUT $canonize, "_SOURCES = $_.cpp\n";
}

print OUT "\n";

#foreach (@test_src) {
#    my $canonize = $_;
#    $canonize =~ tr /\-/_/;
#    print OUT $canonize,
#    '_LDADD = $(top_builddir)/src/libgoose.la', "\n";
#}

close(OUT);
print STDERR "Done\n";


# $Id$


// This is -*- C++ -*-
// $Id$

/* tiny.cpp
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#include <config.h>
#include <iostream>
#include <Random.h>
#include <RealSet.h>
#include <runs.h>


main()
{
  
  Random rng;
  int abbr=0;
  const int N = 20000;

  for(int j=0; j<N; ++j) {

    RunsCount r;
    for(int i=0; i<20; ++i) {
      r.add(rng.random_bool());
    }

    if (r.p_approx() < 0.05)
      ++abbr;

    if (j % 1000 == 0)
      cout << j << endl;
  }
  double p = abbr/(double)N;
  double sd = sqrt(p*(1-p)/N);
  cout << endl << p-2*sd << " " << p << " " << p+2*sd << endl;
    
}




// $Id$

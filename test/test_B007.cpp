// This is -*- C++ -*-
// $Id$

#include <iostream.h>
#include <math.h>
#include <Random.h>
#include <specfns.h>

// for simplicity, we just make this a big array of doubles
// (values taken from the CRC tables)

double values[] = {
    2, 1, 1, 0, 0.5,
    2, 1, 1, 1, 1.0,

    4, 1, 1, 0, 0.75,
    4, 1, 1, 1, 1.0,
    4, 2, 1, 0, 0.5,
    4, 2, 1, 1, 1.0,
    4, 2, 2, 0, 0.166667,
    4, 2, 2, 1, 0.833333,
    4, 2, 2, 2, 1.0,
    4, 3, 1, 0, 0.25,
    4, 3, 1, 1, 1.0,
    4, 3, 2, 1, 0.5,
    4, 3, 2, 2, 1.0,
    4, 3, 3, 2, 0.75,
    4, 3, 3, 3, 1.0,

    8, 6, 3, 3, 1.0,
    8, 6, 4, 2, 0.214286,
    8, 6, 4, 3, 0.785714,
    8, 6, 4, 4, 1.0,
    8, 6, 5, 3, 0.357143,
    8, 6, 5, 4, 0.892857,
    8, 6, 5, 5, 1.0,
    8, 6, 6, 4, 0.535714,
    8, 6, 6, 5, 0.964286,
    8, 6, 6, 6, 1.0,

    10, 6, 4, 1, 0.119048,
    10, 6, 4, 2, 0.547619,
    10, 6, 4, 3, 0.928571,

    10, 6, 6, 3, 0.452381,
    10, 6, 6, 4, 0.880952,
    10, 6, 6, 5, 0.995238,
    10, 6, 6, 6, 1.0,
    10, 7, 1, 0, 0.3,
    10, 7, 1, 1, 1.0,
    10, 7, 2, 0, 0.066667,
    10, 7, 2, 1, 0.533333,
    10, 7, 2, 2, 1.0,
    10, 7, 3, 0, 0.008333,
    
    -1 };
    
main()
{

  cout << "Comparing values calculated by hypergeometric_cdf() to table values." << endl;
  int i=0;
  while (values[i] != -1) {
    unsigned N = (unsigned)values[i];
    unsigned r = (unsigned)values[i+1];
    unsigned n = (unsigned)values[i+2];
    unsigned x = (unsigned)values[i+3];
    double p = values[i+4];

    double calc_p = hypergeometric_cdf(x,r,n,N);
    double err = fabs(p - calc_p);
    
    cout << N << " " << r << " " << n << " " << x << " : ";
    cout << p << " vs " << calc_p << " (" << err << ")" << " ";
    if (err > 1e-6)
      cout << "failed";
    else
      cout << "ok";
    cout << endl;

    i += 5;
  }

  cout << endl;

  cout << "Testing inverse relationship between hypergeometric_cdf()\nand inv_hypergeometric_cdf()." << endl;
  for(unsigned N=1; N<35; ++N)
    for(unsigned n=0; n<N; ++n)
      for(unsigned r=0; r<N; ++r) {
	unsigned first_x = n+r>N ? n+r-N : 0;
	unsigned min_rn = n<r ? n : r;
	for(unsigned x=first_x; x<min_rn; ++x) {
	  double p = hypergeometric_cdf(x,n,r,N);
	  unsigned xx = inv_hypergeometric_cdf(p,n,r,N);
	  if (x != xx) {
	    cout << N << " " << n << " " << r;
	    cout << " : ";
	    cout << x << " vs " << xx;
	    cout << endl;
	  }
	}
      }
}





// $Id$

#!/usr/bin/perl -w
# $Id$


my $bad_count = 0;

sub check_source {
    my $filename = shift;

    ## we exempt test files
    return if $filename =~ /^test\/test_.+\.cpp$/;

    ## we also exempt the contents of spenfns
    return if $filename =~ /specfns/;

    open(IN, $filename) || die "Can't open $filename";
    my ($have_wrong_emc_license, $old_gpl, $lgpl, $no_sign_of_gpl) = (0,0,0,1);
    while (<IN>) {

	## This is in the confidentiality notices in my company's
	## proprietary code.
	$have_wrong_emc_license=1 if /Confidential Proprietary/ ||
	    /Exclusively Owned/;

	## These are fragments of the GPL and LGPL.
	$no_sign_of_gpl=0 if /You should have receieved a copy/ ||
	    /GNU.*General Public/;
	$lgpl=1 if /Library General/;

	## Old copies of the (L)GPL have the wrong address for the FSF
	$old_gpl=1 if /675 Mass Ave/;
    }
    if ($have_wrong_emc_license || $no_sign_of_gpl || !$lgpl || $old_gpl) {
	printf("%25s: ", $filename);
	print "[EMC Secret] " if $have_wrong_emc_license;
	print "[No GPL] " if $no_sign_of_gpl;
	print "[not LGPL]" if !$lgpl;
	print "[Out-of-date GPL] " if $old_gpl;
	print "\n";

	++$bad_count;
    }
}

print "Checking All Source Code for Proper Licenses...\n\n";
foreach (<src/*/*.{h,cpp,scm}>) { check_source($_) }



if ($bad_count) {
    printf "\n%d %s to be changed.\n", $bad_count, 
    $bad_count == 1 ? "file needs" : "files need";
} else {
    print "Licenses OK.\n";
}

# $Id$

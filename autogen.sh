#!/bin/sh

echo " "
echo "Checking on licenses..."
echo " "

./license-check.pl

echo " "
echo "Launching autogen sequence."
echo " "

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

PKG_NAME="Goose"

(test -f $srcdir/configure.in \
  && test -f $srcdir/goose-config.in) || {
    echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
    echo " top-level $PKG_NAME directory"
    exit 1
}

. $srcdir/macros/autogen.sh
